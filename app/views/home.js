var HomeView = Backbone.View.extend({

	template: _.template( $('#home').html(), {
			title: "Rodrigo Dias",
			subheading: "Freelance Web Developer living in London",
			body: "I do all sorts of projects that involve online and offline software. I'm fascinated by challenges that require skill and new technologies and I like to apply the right tools for the right job.",
			icons: [
				{
					file: "img/icons/world_white.png",
					text: "Making the world a better place to live in. This is extremely important to me and its a criteria in my choices of projects.",
				},
				{
					file: "img/icons/cloud2.png",
					text: "Getting your place on the coud is something I help you achieve.",
				},
				{
					file: "img/icons/crane.png",
					text: "A programmer is essentially someone who builds castles out of thin air. Give me a blueprint and I'll make it become an attraction like the Taj Mahal.",
				},
				{
					file: "img/icons/mail.png",
					text: "I'm always available to get in touch via email, phone, skype or pidgin-posts. Just drop me a line on my <a href='#/contacts'>contacts page</a> and I'll get back to you within 24h.",
				},
			]
		}),

	el: '#app',

	render: function(){
		this.$el.html(this.template);
		return this;
	}


});