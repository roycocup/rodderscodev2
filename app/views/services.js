var ServicesView = Backbone.View.extend({
	template: _.template( $('#services').html(), {
		title: "Services",
		subheading: "What I do ",
		body: "My friend... I can do anything... ",
		services: [
			{
				icon: "img/icons/world.png", 
				title: "Webdevelopment",
				text: "Webdevelopment is the process in which I get one of your ideas and turn them into visible webspace."
			},
			{
				icon: "img/icons/settings.png", 
				title: "Application development",
				text: "This is a different type of programming where the aim is to build a tool that may or may not live on the web."
				+ " The key factor here is that there will be users for this tool."
			},
			{
				icon: "img/icons/data.png", 
				title: "Hosting",
				text: "I can host your website for you if your need a reliable place to host your website."
			},
			{
				icon: "img/icons/diamond.png", 
				title: "Design",
				text: "I am not a designer so I work with one or two designers that I usually hire depending on the type of work I have in hands."
				+" That tends to make life easier for both me and my client since we have a professional hand overlooking this side of things."
			},
			{
				icon: "img/icons/bubble.png", 
				title: "Scoping",
				text: "I can scope new projects and give you an insight on the project from a expert technical standpoint in plain english."
			},
			{
				icon: "img/icons/heart.png", 
				title: "Redevelop",
				text: "If you need to re-do your website, or you need an add on or even if your last developer has left things in a mess, let me give you a hand - I love that sort of challenges."
			},

		]
	}),
	
	el: '#app',

	render: function(){
		this.$el.html(this.template);
		return this;
	}

});
