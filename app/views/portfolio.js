var PortfolioView = Backbone.View.extend({

	template: _.template( $('#portfolio').html(), {
			title: "Portfolio",
			subheading: "",
			images: [
				{
					file:'img/portfolio/rodrigodias1.jpg',
					caption:'first image',
					link: 'http://www.rodderscode.co.uk',
					slug: 'rodderscode',
				},
				{
					file:'img/portfolio/alldajobs1.jpg',
					caption:'Job Board that taps into the xml feeds of other sites as a job board digest. Still a work in progress.',
					link: 'http://www.alldajobs.co.uk',
					slug: 'alldajobs',
				},
				{
					file:'img/portfolio/isabella1.jpg',
					caption:'Isabella is an author and an ilustrator',
					link: 'http://www.isabellathermes.com',
					slug: 'isabella',
				},
				{
					file:'img/portfolio/takenote1.jpg',
					caption:'A groupon look-alike built on Code Igniter/Mysql/Apache. Backend includes payment gateway (sage), credits and payment managments',
					link: 'http://www.onurbantribe.com',
					slug: 'onurbantribe',
				},
				{
					file:'img/portfolio/sunshine1.jpg',
					caption:'A very clever project that incetivates people to produce good deeds and post them online (Yii/Mysql/Apache)',
					link: 'http://www.sunshinebank.com',
					slug: 'sunshinebank',
				},
				{
					file:'img/portfolio/kiosk1.png',
					caption:'Cruiseline Intranet - Running on Flex frontend and Php/SqlServer backend, this is a the automatic photo kiosk running over 25 cruiselines all over the world thst can order prints and design photobooks for each passenger',
					link: '',
					slug: 'kiosk',
				},
				{
					file:'img/portfolio/mmafightdb.jpg',
					caption:'A statistical website for MMA. This site is averaging 1500 unique visitors per day.',
					link: 'http://www.mmafightdb.com',
					slug: 'mmafightdb',
				},
				{
					file:'img/portfolio/mentor.jpg',
					caption:'Mentor International website where a cronjob would link to Irix recruitment software to keep a job board always up to date (Modx/Mysql/Apache)',
					link: 'http://www.mentorinternational.com',
					slug: 'mentor',
				},
				{
					file:'img/portfolio/nyp1.jpg',
					caption:'Migration of 30.000 articles from a non-wp site into this New York Press website (Wordpress/PHP/SQL/Server Admin).',
					link: 'http://www.nyp.com',
					slug: 'nyp',
				},
			]
		}),

	el: '#app',

	render: function(){
		this.$el.html(this.template);
		return this;
	}


});