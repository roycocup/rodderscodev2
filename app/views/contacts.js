var ContactsView = Backbone.View.extend({

	template: _.template( $('#contacts').html(), {
			title: "Contacts",
			subheading: "Get in touch!",
			body: "I generally come back to you in a few hours. ",
			email: "rodrigo@rodderscode.co.uk",
			phone: "07596439645",
			skype: "roycocup",
			me: 'img/me2.jpg'
		}),

	el: '#app',

	render: function(){
		this.$el.html(this.template);
		return this;
	}


});