var AboutView = Backbone.View.extend({

	template: _.template( $('#about').html(), {
			title: "About me",
			subheading: "More stuff that you may (or may not) want to know about me",
			body: "This is where I hang my some of my medals of achievement.",
			stories:[
				{
				
					text:"I'm a Zend Certified Engineer. " +
					"I'm happy to say that I belong to the group of only just over 6000 PHP programmers in the world to hold such certificate."+ 
					"Click <a href='http://www.zend.com/en/yellow-pages#show-ClientCandidateID=ZEND012822' target='_blank'>here</a> to see my profile in Zend Yellow Pages. ",
					image:"img/zce.gif",
					image_width: "50px",
					image_float: "left",
					image_margin: "right",
				},
				{
					text:"Whilst working for Pilotbean, I was part of the team that won a public competition entitled Sunshine in Kent. ",
					image: "img/kent.jpg",
					image_width: "200px",
					image_float: "right",
					image_margin: "left",
				},
				{
				
					text:"I have constructed a game", 
					image:"img/zce.gif",
					image_width: "50px",
					image_float: "left",
					image_margin: "right",
				},
				{
					text:"I look like this. ",
					image: "img/me.jpg",
					image_width: "200px",
					image_float: "right",
					image_margin: "left",
				},

			]
		}),

	el: '#app',

	render: function(){
		this.$el.html(this.template);
		return this;
	}


});