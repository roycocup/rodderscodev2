var WorkView = Backbone.View.extend({
	template: _.template( $('#work').html(), {
		title: "Work",
		subheading: "How I do it",
		body: "I like to work in all aspects of an application:",
		phases: [
			{
				text:"Briefing",
				image: "img/icons/pen.png",
			},
			{
				text:"Design",
				image: "img/icons/diamond.png",
			},
			{
				text:"Build",
				image: "img/icons/data.png",
			},
			{
				text:"Test",
				image: "img/icons/fire.png",
			},
			{
				text:"Deploy",
				image: "img/icons/heart.png",
			},
		]
	}),
	
	el: '#app',

	render: function(){
		this.$el.html(this.template);
		return this;
	}

});
