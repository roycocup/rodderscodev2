var AppRouter = Backbone.Router.extend({
	routes: {
		"": "home", 
		"work": "work",
		"about": "about",
		"portfolio": "portfolio",
		"more": "more",
		"services": "services",
		"contacts": "contacts",
	},
});

var app_router = new AppRouter();

app_router.on('route:home', function(){
	var view = new HomeView();
	view.render();
});

app_router.on('route:work', function(){
	var view = new WorkView();
	view.render();
});

app_router.on('route:about', function(){
	var view = new AboutView();
	view.render();
});

app_router.on('route:portfolio', function(){
	var view = new PortfolioView();
	view.render();
});

app_router.on('route:more', function(){
	var view = new MoreView();
	view.render();
});

app_router.on('route:services', function(){
	var view = new ServicesView();
	view.render();
});

app_router.on('route:contacts', function(){
	var view = new ContactsView();
	view.render();
});

$(function(){
	Backbone.history.start();	
});
